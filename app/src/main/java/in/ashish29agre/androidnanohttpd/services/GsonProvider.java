package in.ashish29agre.androidnanohttpd.services;

import com.google.gson.Gson;

/**
 * Created by ymedia on 11/1/16.
 */
public class GsonProvider {
    private static GsonProvider ourInstance = new GsonProvider();

    public static GsonProvider getInstance() {
        return ourInstance;
    }

    private Gson gson;

    private GsonProvider() {
        gson = new Gson();
    }

    public Gson getGson() {
        return gson;
    }
}
